/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#include "swis.h"

struct tfile
{
  unsigned char c;  /* Read ahead */
  FILE *fp;         /* Input file */
};

static int
_next_char_from_file (void *data)
{
  struct tfile *file = data;
  if (file->c)
    {
      int c = file->c;
      file->c = 0;
      return c;
    }
  return fgetc (file->fp);
}

static unsigned *wordbuf = NULL;
static size_t wordsize = 0;
static size_t wordlen = 0;
#define new_word() wordlen = 0

static void
add_char (unsigned wc)
{
  if (!wordbuf)
    {
      wordsize = 16;
      wordbuf = xcalloc (wordsize, sizeof wordbuf[0]);
    }
  
  if (wordlen == wordsize)
    wordbuf = x2nrealloc (wordbuf, &wordsize, sizeof wordbuf[0]);
  wordbuf[wordlen++] = wc;
}

const unsigned *
fget_word (FILE *fp, int (*ignore) (void *data, FILE *fp, int c), void *data)
{
  int c;

  new_word ();
  while ((c = fgetc (fp)) != EOF)
    {
      if (c < 0x80)
	{
	  if (ignore && ignore (data, fp, c))
	    continue;
	  
	  if (isalnum (c))
	    add_char (tolower (c));
	  else
	    {
	      add_char (0);
	      return wordbuf;
	    }
	}
      else
	{
	  struct tfile tfile;
	  int rc;
	  unsigned wc;
	  
	  tfile.c = c;
	  tfile.fp = fp;

	  rc = swis_utf8_mbtowc_internal (&tfile, _next_char_from_file, &wc);
	  if (rc < 0)
	    {
	      /* FIXME: don't know what to do */
	      error (0, errno, "error converting UTF-8 string");
	      return NULL;
	    }

	  add_char (swis_utf8_tolower (wc));
	}
    }
  if (wordlen)
    {
      add_char (0);
      return wordbuf;
    }
  return NULL;
}
