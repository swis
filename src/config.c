/* This file is part of SWIS
   Copyright (C) 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#include "swis.h"
#include "grecs-locus.h"

char *conffile = SYSCONFDIR "/swis.conf";
int lint_mode;
int preprocess_only;

int
assert_string_arg (grecs_locus_t *locus,
		   enum grecs_callback_command cmd,
		   const grecs_value_t *value)
{
  if (cmd != grecs_callback_set_value)
    {
      grecs_error (locus, 0, _("Unexpected block statement"));
      return 1;
    }
  if (!value || value->type != GRECS_TYPE_STRING)
    {
      grecs_error (&value->locus, 0, _("expected scalar value as a tag"));
      return 1;
    }
  return 0;
}

static void
_print_diag (grecs_locus_t const *locus, int err, int errcode, const char *msg)
{
  fflush (stdout);
  if (!locus)
    fprintf (stderr, "%s: ", program_name);
  if (locus)
    {
      YY_LOCATION_PRINT (stderr, *locus);
      fprintf (stderr, ": ");
    }
  if (!err)
    fprintf (stderr, "warning: ");
  fprintf (stderr, "%s", msg);
  if (errcode)
    fprintf (stderr, ": %s", strerror (errno));
  fputc ('\n', stderr);
}

void
config_init()
{
  char *v = getenv ("SWIS_CONFIG");

  if (v)
    conffile = v;
  grecs_include_path_setup (DEFAULT_VERSION_INCLUDE_DIR,
			    DEFAULT_INCLUDE_DIR, NULL);
  grecs_preprocessor = DEFAULT_PREPROCESSOR;
  grecs_log_to_stderr = 1;
  grecs_print_diag_fun = _print_diag;
}

