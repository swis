%{
/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#include "swis.h"
#include "gram.h"

static struct
{
  int wordc;
  int wordi;
  char **wordv;
  char *string;
  size_t size;
} in;
 
#define YY_INPUT(buf,result,max_size)					\
  do									\
    {									\
      result = in.size < max_size ? in.size : max_size;			\
      memcpy (buf, in.string, result);					\
      in.string += result;						\
      in.size -= result;						\
    }									\
 while (0)

%}

%option 8bit
WS [ \t]+
PR [^ \t\n\"()]
WORD {PR}{PR}*
%s QUOTE
%%
\+              return T_AND; 
<INITIAL>\|     return T_OR;
-               return T_NOT;		
{WORD} {
                yylval.word = grecs_strdup (yytext);
                return T_WORD;
}
<INITIAL>\" { BEGIN (QUOTE); return yytext[0]; }
<QUOTE>\"   { BEGIN (INITIAL); return yytext[0]; }
"("|")"     return yytext[0];
.            ;

%%

int
yywrap ()
{
  if (in.wordi == in.wordc)
    return 1;
  in.string = in.wordv[in.wordi++];
  in.size = strlen(in.string);
  return 0;
}

void
lex_init (int argc, char **argv)
{
  in.wordc = argc;
  in.wordv = argv;
  in.wordi = 0;
  yywrap();
}


