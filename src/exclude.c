/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#include "swis.h"
#include "regex.h"

struct exclude_pattern
{
  struct exclude_pattern *next;
  char *pattern;
  regex_t re;
};

struct exclude_pattern *exclude_list;

int regex_flags = REG_EXTENDED;

void
register_exclude_regexp (const char *str)
{
  int rc;
  regex_t re;
  struct exclude_pattern *ep;
  
  rc = regcomp (&re, str, regex_flags);
  if (rc)
    {
      char errbuf[512];
      regerror (rc, &re, errbuf, sizeof(errbuf));
      error (1, 0, "cannot compile regex: %s", errbuf);
    }
  
  ep = xmalloc (sizeof ep[0] + strlen (str) + 1);
  ep->next = exclude_list;
  exclude_list = ep;
  strcpy (ep->pattern = (char *) (ep + 1), str);
  ep->re = re;
}

void
read_exclude_regexps (const char *filename)
{
  FILE *fp;
  size_t size = 0;
  char *buf = NULL;
  
  fp = fopen (filename, "r");
  if (!fp)
    error (1, errno, "cannot open exclude list file %s", filename);

  while (getline (&buf, &size, fp) > 0)
    {
      int len = strlen (buf);
      char *p;
      if (buf[len-1] == '\n')
        buf[len-1] = '\0';
      for (p = buf; *p && isspace (*p); p++)
	;
      if (*p == 0 || *p == '#')
	continue;
      register_exclude_regexp (p);
    }
  free (buf);
}

int
excluded_word_p (const char *word)
{
  struct exclude_pattern *ep;
  for (ep = exclude_list; ep; ep = ep->next)
    {
      if (regexec (&ep->re, word, 0, NULL, 0) == 0)
	return 1;
    }
  return 0;
}

