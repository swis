/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#include "swis.h"
#include "swis/backend.h"

char *input_name;
unsigned input_line;
FILE *input;
int debug_level;
char *backend_name;

struct swis_backend_module *module;
swis_backend_handle_t bh;
unsigned long word_pos;

void
change_url (const char *url)
{
  if (module->swis_change_url && module->swis_change_url (bh, url))
    error (1, 0, "swis_change_url failed");
  word_pos = 0;
}

void
store_text (const char *text)
{
  if (module->swis_store_text && module->swis_store_text (bh, text))
    error (1, 0, "swis_store_text failed");
}

void
store_word (char *word, unsigned long off)
{
  if (module->swis_store_word &&
      module->swis_store_word (bh, word, off, word_pos))
    error (1, 0, "swis_store_word failed");
  word_pos++;
}

int
parse_record (char *buf, char **pword, unsigned long *pnum)
{
  char *p;
  
  *pnum = strtoul (buf, &p, 10);
  if (*p != ' ')
    {
      error_at_line (0, 0, input_name, input_line, "malformed input: %s", buf);
      return 1;
    }
  *pword = p + 1;
  return 0;
}

/* Main loop */
void
read_loop ()
{
  char *buf = NULL;
  size_t size = 0;
  char *word;
  unsigned long num;
  
  while (getline (&buf, &size, input) > 0)
    {
      int len;

      input_line++;

      len = strlen (buf);
      if (buf[len-1] == '\n')
	buf[len-1] = 0;
      
      if (buf[0] == '>')
	{
	  switch (buf[1])
	    {
	    case ' ':
	      change_url (buf + 2);
	      break;

	    case '*':
	      store_text (buf + 2);
	      break;

	    default:
	      error_at_line (0, 0, input_name, input_line,
			     "unexpected input: %s", buf);
	    }
	}
      else if (parse_record (buf, &word, &num) == 0)
	store_word (word, num);
    }
  free (buf);
}

static int
readinput (char *file)
{
  static int stdin_used = 0;

  if (strcmp (file, "-") == 0)
    {
      if (stdin_used)
	error (0, 0, "stdin has already been read");
      input = stdin;
      stdin_used = 1;
      input_name = "<stdin>";
    }
  else
    {
      input = fopen (file, "r");
      input_name = file;
    }
  input_line = 0;
  if (!input)
    {
      error (0, errno, "can't open file %s", file);
      return 1;
    }
  read_loop ();
  fclose (input);
  return 0;
}

#include "store-cli.h"
#include "store-cfg.h"

int
main (int argc, char **argv)
{
  int index;
  struct grecs_node *tree, *subtree;
  char buf[256];
  
  set_program_name (argv[0]);
  config_init ();
  index = parse_options (argc, argv);
  
  if (preprocess_only)
    exit (grecs_preproc_run (conffile, grecs_preprocessor) ? EX_CONFIG : 0);
  tree = grecs_parse (conffile);
  if (!tree)
    exit (EX_CONFIG);

  config_finish (tree);
  if (!backend_name)
    error (1, 0, "backend name not specified");
  if (lint_mode)
    exit (0);

  swis_loader_init ();
  module = swis_backend_load (backend_name);
  if (!module)
    exit (EX_UNAVAILABLE);

  snprintf (buf, sizeof (buf), "backend-config=\"%s\"", backend_name);
  subtree = grecs_find_node (tree, buf);
  if (subtree)
    {
      subtree = subtree->down;
    }
  
  bh = module->swis_open (subtree, SWIS_MODE_WRITE);
  if (!bh)
    exit (EX_UNAVAILABLE);
  
  argc -= index;
  argv += index;

  if (argc == 0)
    {
      if (readinput("-"))
	return 1;
    }
  else
    {
      while (argc--)
	readinput (*argv++);
    }
  module->swis_close (bh);
  return 0;
}
