/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#include "swis.h"
#include "swis/utf8.h"
#include "hash.h"

static Hash_table *badword_table;

static size_t
hash_utf8w_hasher (void const *data, unsigned n_buckets)
{
  const unsigned *s = data;
  return swis_mbutf8_hash_string (s, n_buckets);
}

static bool
hash_utf8w_compare (void const *data1, void const *data2)
{
  unsigned const *s1 = data1;
  unsigned const *s2 = data2;
  return swis_mbutf8_strcmp (s1, s2) == 0;
}

void
hash_utf8w_free (void *data)
{
  free (data);
}

void
register_badword (const unsigned *s)
{
  unsigned *ret;
  unsigned *nw = swis_mbutf8_strdup (s);
  
  if (! ((badword_table
	  || (badword_table = hash_initialize (0, 0,
					       hash_utf8w_hasher,
					       hash_utf8w_compare,
					       hash_utf8w_free)))
	 && (ret = hash_insert (badword_table, nw))))
    xalloc_die ();

  if (ret != nw)
    free (nw);
}

int
badword_p (const unsigned *s)
{
  return badword_table && hash_lookup (badword_table, s) != NULL;
}

void
read_badwords (char *name)
{
  const unsigned *w;
  FILE *fp = fopen (name, "r");

  if (!fp)
    {
      error (0, errno, "cannot open %s for reading", name);
      return;
    }
  
  while ((w = fget_word (fp, NULL, NULL)))
    register_badword (w);

  fclose (fp);
}
  

