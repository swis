%{
/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#include "swis.h"
#include "gram.h"
#include "swis/query.h"
  
struct swis_node *query_tree;
struct swis_node *create_node (enum swis_node_type type);
%}
%token <word> T_WORD 
%left T_OR
%left T_AND
%left T_NOT
%type <node> expr or_expr and_expr not_expr term word
%type <nodelist> sequence

%union {
  char *word;
  struct swis_node *node;
  struct nodelist
  {
    struct swis_node *head;
    struct swis_node *tail;
  } nodelist;
};

%%
input   : expr
          {
	    query_tree = $1;
	  }
        ;

expr    : or_expr
        ;

or_expr : and_expr
        | or_expr T_OR and_expr
          {
	    $$ = create_node (swis_node_op);
	    $$->v.op.opcode = swis_op_or;
	    $$->v.op.arg[0] = $1;
	    $$->v.op.arg[1] = $3;
	  }
        ;

and_expr: not_expr
        | and_expr and not_expr
          {
	    $$ = create_node (swis_node_op);
	    $$->v.op.opcode = swis_op_and;
	    $$->v.op.arg[0] = $1;
	    $$->v.op.arg[1] = $3;
	  }
        ;

and     : /* empty */
        | T_AND
        ;

not_expr: term
        | T_NOT term
          {
	    $$ = create_node (swis_node_op);
	    $$->v.op.opcode = swis_op_not;
	    $$->v.op.arg[0] = $2;
	  }
        ;

term    : word
        | '"' sequence '"'
          {
	    if ($2.head == $2.tail)
	      {
		$$ = $2.head->v.seq.this;
		free ($2.head);
	      }
	    else
	      $$ = $2.head;
	  }
        | '(' expr ')'
          {
	    $$ = $2;
	  }
        ;

sequence: word
          {
	    $$.head = $$.tail = create_node (swis_node_seq);
	    $$.head->v.seq.this = $1;
	    $$.head->v.seq.next = NULL;
	  }
        | sequence word
          {
	    struct swis_node *t = create_node (swis_node_seq);
	    t->v.seq.this = $2;
	    t->v.seq.next = NULL;
	    
	    $1.tail->v.seq.next = t;
	    $1.tail = t;
	    $$ = $1;
	  }
        ;

word    : T_WORD
          {
	    $$ = create_node (swis_node_word);
	    $$->v.word = $1;
	  }
        ;

%%
int
yyerror (char *s)
{
  error (1, 0, "%s", s);
}

struct swis_node *
create_node (enum swis_node_type type)
{
  struct swis_node *np = grecs_zalloc (sizeof np[0]);
  np->type = type;
  return np;
}
