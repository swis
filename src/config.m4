dnl This file is part of SWIS
dnl Copyright (C) 2012 Sergey Poznyakoff
dnl 
dnl SWIS is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 3, or (at your option)
dnl any later version.
dnl
dnl SWIS is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with SWIS.  If not, see <http://www.gnu.org/licenses/>.
divert(-1)

changecom(/*,*/)

define(`quote', `ifelse(`$#', `0', `', ``$*'')')

define(`__SWIS_STMT_NAME',``SWIS_STMT_'translit($1,`a-z-',`A-Z_')')

define(`__SWIS_STMT',`dnl
pushdef(`macro',defn(__SWIS_STMT_NAME($1)))dnl
ifelse(quote(macro),,`GRECS_INAC',`macro')`'dnl
popdef(`macro')')

define(`SWIS_STMT',`dnl
define(__SWIS_STMT_NAME($1),`$2')')

define(`SWIS_CONFIG',`
pushdef(`kwname',`translit($1,`-',`_')_kw')
static struct grecs_keyword kwname[] = {
  { "module-load-path",
     N_("path"), N_("List of directories searched for backend modules."),
    grecs_type_string, GRECS_LIST|__SWIS_STMT(module-load-path) },
  { "prepend-load-path",
     N_("path"),
     N_("List of directories searched for backend modules prior to "
         "the default module directory"),
    grecs_type_string, GRECS_LIST|__SWIS_STMT(prepend-load-path) },
  { "debug", NULL, N_("Set debug level"),
    grecs_type_bool, __SWIS_STMT(debug) },
  { "source-info", NULL,
    N_("Preserve input source information for error messaging."),
    grecs_type_bool, __SWIS_STMT(source-info) },
  { "directory", NULL, N_("Source directory."),
    grecs_type_string, __SWIS_STMT(directory)  },
  { "base-url", NULL, N_("Set base URL."),
    grecs_type_string, __SWIS_STMT(base-url) },
  { "file-pattern", NULL, N_("Set file pattern"),
    grecs_type_string, GRECS_AGGR|__SWIS_STMT(file-pattern) },
  { "find-options", NULL, N_("Pass additional options to find"),
    grecs_type_string, GRECS_AGGR|__SWIS_STMT(find-options) },
  { "backend", N_("name"), N_("Select backend; see also backend-config."),
    grecs_type_string, __SWIS_STMT(backend) },
  { "badwords", N_("file"), N_("Read words to be ignored from <file>."),
    grecs_type_string, GRECS_LIST|GRECS_AGGR|__SWIS_STMT(badwords) },
  { "min-length", NULL, N_("Set minimal valid word length."),
    grecs_type_size, __SWIS_STMT(min-length) },
  { "full-text", NULL, N_("Create full text index."),
    grecs_type_bool, __SWIS_STMT(full-text) },
  { "exclude", N_("pattern"), N_("Set exclude pattern."),
    grecs_type_string, GRECS_LIST|GRECS_AGGR|__SWIS_STMT(exclude) },
  { "exclude-from", N_("file"), N_("Read exclude patterns from <file>."),
    grecs_type_string, GRECS_LIST|GRECS_AGGR|__SWIS_STMT(exclude-from) },
  { "backend-config",
    N_("ifdef(`SWIS_BACKEND_CONFIG_TAG',SWIS_BACKEND_CONFIG_TAG,name: string)"),
    N_("ifdef(`SWIS_BACKEND_CONFIG_DESCR',SWIS_BACKEND_CONFIG_DESCR,dnl
Configure backend; see particular backends for available statements.)"),
    grecs_type_section, __SWIS_STMT(backend-config) },
  { NULL }
};

void
config_finish (struct grecs_node *tree)
{
  if (grecs_tree_process (tree, kwname))
    exit (EX_CONFIG);
}

void
config_help ()
{
  static char docstring[] =
    N_("Configuration file structure for $1.\n"
       "For more information, use `info swis configuration'.");
  grecs_print_docstring (docstring, 0, stdout);
  grecs_print_statement_array (kwname, 1, 0, stdout);
}

popdef(`kwname')
')
divert(0)dnl
/* -*- buffer-read-only: t -*- vi: set ro:
 * THIS FILE IS GENERATED AUTOMATICALLY.  PLEASE DO NOT EDIT.
 */
