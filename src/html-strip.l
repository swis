%{
/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#include "swis.h"
#include <iconv.h>

char inbuf[80];
char outbuf[80];
size_t idx;
#define INVALID_ICONV_CD (iconv_t) -1 
iconv_t cd = INVALID_ICONV_CD;

static void
parse_content_type ()
{
  char *start, *p;
  size_t len;
  char *buf;

  for (len = 1, start = yytext + yyleng - 2;
       start > yytext && start[-1] != '"'; start--, len++)
    ;

  len = strlen (start) - 1;
  buf = xmalloc (len + 1);
  memcpy (buf, start, len);
  buf[len] = 0;
  p = strchr (buf, ';');
  if (p)
    {
      for (p++; *p && isspace (*p); p++)
	;
      if (strncasecmp (p, "charset=", 8) == 0)
	{
	  p += 8;
	  start = p;
	  while (*p)
	    {
	      if (*p == ';' || isspace (*p))
		{
		  *p = 0;
		  break;
		}
	      p++;
	    }
	  memmove (buf, start, p - start + 1);
	  if (strcasecmp (buf, "utf-8"))
	    {
	      if (yy_flex_debug)
		fprintf (stderr, "enabling conversion %s->%s\n", buf,"utf-8");
	      cd = iconv_open ("UTF-8", buf);
	      if (cd == INVALID_ICONV_CD)
		error (0, errno, "cannot convert from %s", buf);
	    }
	}
    }
  free (buf);
}

int in_body;
 
void
output (const char *str)
{
  if (in_body)
    fputs (str, yyout);
}

 
void
convert_output ()  
{
  char *outptr = outbuf;
  size_t outsize = sizeof (outbuf);
  char *inptr = inbuf;
  size_t insize;
  size_t rc;
	    
  insize = idx;
  rc = iconv (cd, (ICONV_CONST char**)&inptr, &insize, &outptr, &outsize);
  if (outptr != outbuf)
    {
      int saved_errno = errno;
      if (fwrite (outbuf, 1, outptr - outbuf, yyout) < outptr - outbuf)
	error (1, errno, "write error");
      errno = saved_errno;
    }
  if (rc == (size_t) -1)
    {
      if (errno == EILSEQ) 
	error (1, 0, "cannot convert \"%.*s\"", idx, inbuf);
      else if (errno == EINVAL)
	{
	  memmove (inbuf, inptr, insize);
	  idx = insize;
	}
      else if (errno != E2BIG)
	error (1, errno, "cannot convert");
    }
  else
    idx = 0;
}

 
%}

%option case-insensitive 8bit

%x ELEMENT 

WS [ \t]+
%%
<INITIAL>{
      "<meta"{WS}"http-equiv=\"Content-Type\""{WS}"content=\""[^\"]*"\"" {
	parse_content_type ();
	BEGIN (ELEMENT);
      }
      "<body"    { in_body = 1; BEGIN (ELEMENT); }
      "</body"   { in_body = 0; BEGIN (ELEMENT); }
      "<"        { BEGIN (ELEMENT); }
      "&middot;" output ("·");
      "&Agrave;" output (yytext[1] == 'A' ? "À" : "à");
      "&Aacute;" output (yytext[1] == 'A' ? "Á" : "á");
      "&Acirc;"  output (yytext[1] == 'A' ? "Â" : "â"); 
      "&Atilde;" output (yytext[1] == 'A' ? "Ã" : "ã"); 
      "&Auml;"   output (yytext[1] == 'A' ? "Ä" : "ä");
      "&Aring;"  output (yytext[1] == 'A' ? "Å" : "å"); 
      "&AElig;"  output (yytext[1] == 'A' ? "Æ" : "æ"); 
      "&Ccedil;" output (yytext[1] == 'C' ? "Ç" : "ç"); 
      "&Egrave;" output (yytext[1] == 'E' ? "È" : "è"); 
      "&Eacute;" output (yytext[1] == 'E' ? "É" : "é"); 
      "&Ecirc;"  output (yytext[1] == 'E' ? "Ê" : "ê"); 
      "&Euml;"   output (yytext[1] == 'E' ? "Ë" : "ë"); 
      "&Igrave;" output (yytext[1] == 'I' ? "Ì" : "ì"); 
      "&Iacute;" output (yytext[1] == 'I' ? "Í" : "í"); 
      "&Icirc;"  output (yytext[1] == 'I' ? "Î" : "î"); 
      "&Iuml;"   output (yytext[1] == 'I' ? "Ï" : "ï"); 
      "&Dstrok;" output ("Ð");
      "&Ntilde;" output (yytext[1] == 'N' ? "Ñ" : "ñ"); 
      "&Ograve;" output (yytext[1] == 'O' ? "Ò" : "ò"); 
      "&Oacute;" output (yytext[1] == 'O' ? "Ó" : "ó");  
      "&Ocirc;"  output (yytext[1] == 'O' ? "Ô" : "ô"); 
      "&Otilde;" output (yytext[1] == 'O' ? "Õ" : "õ"); 
      "&Ouml;"   output (yytext[1] == 'O' ? "Ö" : "ö"); 
      "&Oslash;" output (yytext[1] == 'O' ? "Ø" : "ø"); 
      "&Ugrave;" output (yytext[1] == 'U' ? "Ù" : "ù"); 
      "&Uacute;" output (yytext[1] == 'U' ? "Ú" : "ú"); 
      "&Ucirc;"  output (yytext[1] == 'U' ? "Û" : "û"); 
      "&Uuml;"   output (yytext[1] == 'U' ? "Ü" : "ü"); 
      "&Yacute;" output (yytext[1] == 'Y' ? "Ý" : "ý"); 
      "&THORN;"  output (yytext[1] == 'T' ? "Þ" : "þ"); 
      "&szlig;"  output ("ß");
      "&Eth;"    output (yytext[1] == 'E' ? "Ð" : "ð");
      "&yuml;"   output ("ÿ");
      "&#"[0-9]{1,5}";" { /* FIXME */; }
      "&nbsp;"   output (" ");
      "&"[^;]*";" output (" ");
      . {
	if (in_body)
	  {
	    if (cd != INVALID_ICONV_CD)
	      {
		inbuf[idx++] = yytext[0];
		convert_output ();
	      }
	    else
	      ECHO;
	  }
      }
}
<ELEMENT>{
      \"[^\"]*\" ; 
      '[^']*'    ;
      ">"        { BEGIN (INITIAL); output (" "); }
      .          ;
}
%%

char **input_file;
int source_info;
int debug_level;

int
open_input ()
{
  if (input_file && *input_file)
    {
      char *name = *input_file++;
      if (name[0] == '-' && name[1] == 0)
        yyin = stdin;
      else
        {
          yyin = fopen (name, "r");
          if (!yyin)
            error (1, errno, "cannot open input file %s", name);
        }
      if (source_info)
        {
           fprintf (yyout ? yyout : stdout, "\n> %s\n", name);
        }
       return 0;
    }
  return 1;
}


int
yywrap()
{
  if (cd != INVALID_ICONV_CD)
    {
      if (idx)
	convert_output ();
      iconv_close (cd);
      cd = INVALID_ICONV_CD;
    }
  return open_input ();
}


#include "html-strip-cli.h"
#include "html-strip-cfg.h"

int
main (int argc, char **argv)
{
  int index;
  struct grecs_node *tree;
  
  set_program_name (argv[0]);
  yy_flex_debug = 0;
  config_init ();
  
  index = parse_options (argc, argv);
  argc -= index;
  argv += index;

  if (preprocess_only)
    exit (grecs_preproc_run (conffile, grecs_preprocessor) ? EX_CONFIG : 0);
  tree = grecs_parse (conffile);
  if (!tree)
    exit (EX_CONFIG);

  config_finish (tree);
  grecs_tree_free (tree);
  if (lint_mode)
    exit (0);

  yy_flex_debug = debug_level_option || debug_level;
  if (tag_option)
    source_info = tag_option;
  
  update_argcv (&argc, &argv);

  if (argc)
    {
      input_file = argv;
      open_input ();
    }
  
  while (yylex ())
    ;
  exit (0);
}


/* Local Variables: */
/* mode: c */
/* eval: (setq buffer-file-coding-system (if window-system 'utf-8 'raw-text-unix) */
/* End: */
