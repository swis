/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#include "swis.h"
#include "swis/utf8.h"

char **input_file;
FILE *input;
FILE *output;
int source_info;
size_t min_length;
int full_text_option;

int
open_input ()
{
  if (input_file && *input_file)
    {
      char *name = *input_file++;
      if (name[0] == '-' && name[1] == 0)
        input = stdin;
      else
        {
          input = fopen (name, "r");
          if (!input)
            error (1, errno, "cannot open input file %s", name);
        }
      return 0;
    }
  return 1;
}

struct obstack stk;
struct header
{
  int visible;
  size_t len;
};

size_t wordcount;

void
flush_stack ()
{
  char *start, *p;
  size_t i;
  size_t offset = 0;
  struct header hdr;

  if (!wordcount)
    return;
  start = obstack_finish (&stk);

  if (full_text_option)
    {
      fputs (">*", output);
      for (i = 0, p = start; i < wordcount; i++)
	{
	  memcpy (&hdr, p, sizeof (hdr));
	  p += sizeof (hdr);
	  fwrite (p, hdr.len, 1, output);
	  fputc (' ', output);
	  p += hdr.len;
	}
      fputc ('\n', output);
    }

  offset = 0;
  for (i = 0, p = start; i < wordcount; i++)
    {
      memcpy (&hdr, p, sizeof (hdr));
      p += sizeof (hdr);
      if (hdr.visible)
	{
	  fprintf (output, "%lu ", (unsigned long) offset);
	  fwrite (p, hdr.len, 1, output);
	  fputc ('\n', output);
	}
      p += hdr.len;
      offset += hdr.len + 1;
    }

  obstack_free (&stk, start);
  wordcount = 0;
}

void
flush_word (const unsigned *wordbuf)
{
  size_t i;
  size_t wordlen;
  char *wbuf;
  size_t wblen;
  size_t wbc;
  struct header hdr;
  
  if (!wordbuf[0])
    return;
  
  wordlen = swis_mbutf8_strlen (wordbuf);
  
  wblen = wordlen * 4;
  wbuf = xmalloc (wblen + 1);
  
  wbc = 0;
  for (i = 0; i < wordlen; i++)
    {
      unsigned char r[4];
      int rc = swis_utf8_wctomb (r, wordbuf[i]);
      if (rc <= 0)
	error (1, errno, "unexpected error converting UTF-8 string");
      memcpy (wbuf + wbc, r, rc);
      wbc += rc;
    }
  wbuf[wbc] = 0;

  hdr.visible = !(wordlen <= min_length ||
		  badword_p (wordbuf) || excluded_word_p (wbuf));
  hdr.len = wbc;
  obstack_grow (&stk, &hdr, sizeof (hdr));
  obstack_grow (&stk, wbuf, wbc);
  wordcount++;

  free (wbuf);
}

int
skip_tag (void *data, FILE *fp, int c)
{
  int *pv = data;
  if (source_info)
    {
      if (*pv && c == '>')
	{
	  flush_stack ();
	  do
	    fputc (c, output);
	  while ((c = fgetc (fp)) != EOF && c != '\n');
	  fputc ('\n', output);
	  *pv = 0;
	  return 1;
	}
      *pv = c == '\n';
    }
  return 0;
}

int
word_split ()
{
  const unsigned *w;
  int after_newline = 1;
  
  while ((w = fget_word (input, skip_tag, &after_newline)))
    flush_word (w);
  flush_stack ();
  return !open_input ();
}


#include "word-split-cli.h"
#include "word-split-cfg.h"

int
main (int argc, char **argv)
{
  int index;
  struct grecs_node *tree;

  set_program_name (argv[0]);
  config_init ();
  index = parse_options (argc, argv);

  argc -= index;
  argv += index;

  if (preprocess_only)
    exit (grecs_preproc_run (conffile, grecs_preprocessor) ? EX_CONFIG : 0);
  tree = grecs_parse (conffile);
  if (!tree)
    exit (EX_CONFIG);

  config_finish (tree);
  grecs_tree_free (tree);
  if (lint_mode)
    exit (0);

  update_argcv (&argc, &argv);

  if (argc)
    {
      input_file = argv;
      open_input ();
    }
  
  if (!input)
    input = stdin;
  if (!output)
    output = stdout;

  obstack_init (&stk);
  
  while (word_split ())
    ;
  
  exit (0);
}

 

  
