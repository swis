/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#include "swis.h"

int filename_terminator = '\n';

enum read_file_list_state  /* Result of reading file name from the list file */
  {
    file_list_success,     /* OK, name read successfully */
    file_list_end,         /* End of list file */
    file_list_zero,        /* Zero separator encountered where it should not */
    file_list_skip         /* Empty (zero-length) entry encountered, skip it */
  };

/* Read from FP a sequence of characters up to FILENAME_TERMINATOR and put them
   into STK.
 */
static enum read_file_list_state
read_name_from_file (FILE *fp, struct obstack *stk)
{
  int c;
  size_t counter = 0;

  for (c = getc (fp); c != EOF && c != filename_terminator; c = getc (fp))
    {
      if (c == 0)
	{
	  /* We have read a zero separator. The file possibly is
	     zero-separated */
	  return file_list_zero;
	}
      obstack_1grow (stk, c);
      counter++;
    }

  if (counter == 0 && c != EOF)
    return file_list_skip;

  obstack_1grow (stk, 0);

  return (counter == 0 && c == EOF) ? file_list_end : file_list_success;
}

struct obstack argv_stk;
int stk_init;
size_t argcount;

void
read_names_from_file (const char *filename)
{
  FILE *fp;
  size_t count = 0, i;
  enum read_file_list_state read_state;
  
  if (!strcmp (filename, "-"))
    {
      //      request_stdin ("-T");
      fp = stdin;
    }
  else
    {
      if ((fp = fopen (filename, "r")) == NULL)
	error (1, errno, "cannot open file %s", filename);
    }

  if (!stk_init)
    {
      stk_init = 1;
      obstack_init (&argv_stk);
    }
  
  while ((read_state = read_name_from_file (fp, &argv_stk)) != file_list_end)
    {
      switch (read_state)
	{
	case file_list_success:
	  argcount++;
	  break;

	case file_list_end: /* won't happen, just to pacify gcc */
	  break;

	case file_list_zero:
	  {
	    size_t size;
	    char *p;
	    
	    error (0, 0, "%s: file name read contains nul character",
		   filename);

	    /* Prepare new stack contents */
	    /* FIXME */
	    size = obstack_object_size (&argv_stk);
	    p = obstack_finish (&argv_stk);
	    for (i = 0; size > 0; size--, p++)
	      {
		if (*p)
		  obstack_1grow (&argv_stk, *p);
		else if (i < argcount)
		  i++;
		else
		  obstack_1grow (&argv_stk, '\n');
	      }
	    obstack_1grow (&argv_stk, 0);
	    count = 1;
	    /* Read rest of files using new filename terminator */
	    filename_terminator = 0;
	    break;
	  }

	case file_list_skip:
	  break;
	}
    }

  fclose (fp);
  argcount += count;
}

void
update_argcv (int *pargc, char ***pargv)
{
  int i, new_argc;
  char **new_argv;
  char *p, *start;

  if (argcount == 0)
    return;
  new_argc = *pargc + argcount + 1;
  new_argv = xmalloc (sizeof (new_argv[0]) * (new_argc + 1));
  memcpy (new_argv, *pargv, sizeof (new_argv[0]) * *pargc);

  start = obstack_finish (&argv_stk);
  for (i = *pargc, p = start; *p; p += strlen (p) + 1, i++)
    new_argv[i] = xstrdup (p);
  new_argv[i] = NULL;

  obstack_free (&argv_stk, NULL);

  *pargv = new_argv;
  *pargc = new_argc;
}
