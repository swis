/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#include "swis.h"
#include "swis/backend.h"
#include <ltdl.h>

struct grecs_list *prepend_load_path;
struct grecs_list *module_load_path; 

typedef int (*grecs_list_iterator_t)(void *, void *);

void
grecs_list_iterate (struct grecs_list *list, grecs_list_iterator_t fun,
		    void *data)
{
  struct grecs_list_entry *ep;

  if (list)
    for (ep = list->head; ep; ep = ep->next)
      {
	grecs_value_t *vp = ep->data;
	if (fun (vp, data))
	  break;
      }
}

/* Load path */
static int
_add_load_dir (void *item, void *unused)
{
  char *str = item;
  return lt_dladdsearchdir (str);
}

void
swis_loader_init ()
{
  lt_dlinit ();
  grecs_list_iterate (prepend_load_path, _add_load_dir, NULL);
  lt_dladdsearchdir (SWIS_MODDIR);
  grecs_list_iterate (module_load_path, _add_load_dir, NULL);
}

#define MODULE_ASSERT(cond)					\
    if (!(cond)) {						\
	lt_dlclose(handle);					\
	error(1, 0, _("%s: faulty module: (%s) failed"),        \
	      name,						\
	      #cond);						\
	return NULL;						\
    }

struct swis_backend_module *
swis_backend_load (const char *name)
{
  lt_dlhandle handle = NULL;
  lt_dladvise advise = NULL;
  struct swis_backend_module *pmod;
  
  if (!lt_dladvise_init (&advise) &&
      !lt_dladvise_ext (&advise) &&
      !lt_dladvise_global (&advise))
    handle = lt_dlopenadvise (name, advise);
  lt_dladvise_destroy (&advise);

  if (!handle)
    {
      error (0, 0, _("cannot load module %s: %s"), name, lt_dlerror ());
      return NULL;
    }

  pmod = (struct swis_backend_module *) lt_dlsym (handle, "module");
  MODULE_ASSERT (pmod);
  MODULE_ASSERT (pmod->swis_version <= SWIS_BACKEND_VERSION);
  MODULE_ASSERT (pmod->swis_open);
  MODULE_ASSERT (pmod->swis_close);
  if (pmod->swis_init)
    pmod->swis_init ();
  return pmod;
}
  
