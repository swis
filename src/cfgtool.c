/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <string.h>
#include <errno.h>
#include "swis.h"

int flags = GRECS_NODE_FLAG_VALUE;
int assignment;

#include "cfgtool-cli.h"
#include "cfgtool-cfg.h"

#define CFGTOOL_OK               0
#define CFGTOOL_NOTFOUND         1 
#define CFGTOOL_SOMENOTFOUND     2

static void
print_ident (char *s)
{
  for (; *s; s++)
    {
      if (strchr ("-.", *s))
	putchar ('_');
      else
	putchar (*s);
    }
}

static void
print_node (struct grecs_node *node)
{
  if (assignment)
    {
      print_ident (node->ident);
      putchar('=');
      putchar('"');
    }
  grecs_print_node (node, flags, stdout);
}

static void
print_node_cont (struct grecs_node *node)
{
  if (assignment)
    putchar (' ');
  else
    putchar ('\n');
  grecs_print_node (node, flags, stdout);
}

int
main (int argc, char **argv)
{
  int index, rc, notfound;
  struct grecs_node *tree, *node;
  char *path;
    
  set_program_name (argv[0]);
  config_init ();
  index = parse_options (argc, argv);

  argc -= index;
  argv += index;

  if (preprocess_only)
    return grecs_preproc_run (conffile, grecs_preprocessor) ? EX_CONFIG : 0;
  tree = grecs_parse (conffile);
  if (!tree)
    return EX_CONFIG;
  config_finish (tree);
  if (lint_mode || argc == 0)
    return CFGTOOL_OK;

  if (argc > 1)
    {
      if (!assignment)
	flags |= GRECS_NODE_FLAG_PATH;
      notfound = CFGTOOL_SOMENOTFOUND;
    }
  else
    notfound = CFGTOOL_NOTFOUND;

  rc = CFGTOOL_OK;
  while (argc--)
    {
      path = *argv++;
      
      node = grecs_find_node (tree, path);
      if (!node)
	{
	  rc = notfound;
	  continue;
	}
      print_node (node);
      
      while ((node = node->next) && (node = grecs_find_node (node, path)))
	print_node_cont (node);
      if (assignment)
	putchar('"');
      putchar ('\n');
    }
  grecs_tree_free (tree);
  return rc;
}
