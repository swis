/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#include "swis.h"
#include "swis/backend.h"
#include "swis/query.h"
#include "search.h"

int debug_level;
char *backend_name;
struct swis_backend_module *module;
swis_backend_handle_t bh;


void
print_node (struct swis_node *node, int level)
{
  if (!node)
    return;
  printf ("%*.*s", level, level, " ");
  switch (node->type)
    {
    case swis_node_word:
      printf ("WORD %s\n", node->v.word);
      break;

    case swis_node_op:
      switch (node->v.op.opcode)
	{
	case swis_op_not:
	  puts ("NOT");
	  break;

	case swis_op_or:
	  puts ("OR");
	  break;

	case swis_op_and:
	  puts ("AND");
	  break;

	default:
	  abort ();
	}
      print_node (node->v.op.arg[0], level + 1);
      print_node (node->v.op.arg[1], level + 1);
      break;

    case swis_node_seq:
      puts ("SEQ");
      for (node; node; node = node->v.seq.next)
	print_node (node->v.seq.this, level + 1);
      break;
      
    default:
      abort ();
    }
}

#include "search-cli.h"
#include "search-cfg.h"

int
main (int argc, char **argv)
{
  int index, rc;
  struct grecs_node *tree, *subtree;
  char buf[256];
  
  set_program_name (argv[0]);
  yydebug = yy_flex_debug = 0;
  config_init ();
  index = parse_options (argc, argv);
  
  if (preprocess_only)
    exit (grecs_preproc_run (conffile, grecs_preprocessor) ? EX_CONFIG : 0);
  tree = grecs_parse (conffile);
  if (!tree)
    exit (EX_CONFIG);

  config_finish (tree);
  if (lint_mode)
    exit (0);

  argc -= index;
  argv += index;

  if (argc)
    lex_init (argc, argv);
  else
    {
      char *x[2];
      char *query = getenv ("QUERY_STRING");
      if (!query)
	error (1, 0, "no query");
      x[0] = query;
      x[1] = NULL;
      lex_init (1, x);
    }

  if (yyparse ())
    return 1;
  
  if (debug_level)
    {
      print_node (query_tree, 0);
      return 0;
    }

  if (!backend_name)
    error (1, 0, "backend name not specified");
  swis_loader_init ();
  module = swis_backend_load (backend_name);
  if (!module)
    exit (EX_UNAVAILABLE);
  if (!module->swis_search)
    error (1, 0, "backend is not suitable for searching");
  snprintf (buf, sizeof (buf), "backend-config=\"%s\"", backend_name);
  subtree = grecs_find_node (tree, buf);
  if (subtree)
    {
      subtree = subtree->down;
    }
  
  bh = module->swis_open (subtree, SWIS_MODE_READ);
  if (!bh)
    exit (EX_UNAVAILABLE);
  
  rc = module->swis_search (bh, query_tree);
  
  module->swis_close (bh);
  return !!rc;
}
