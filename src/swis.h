/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <locale.h>
#include <sysexits.h>
#include "grecs.h"
#include "error.h"
#include "progname.h"
#include "xalloc.h"

#define obstack_chunk_alloc malloc
#define obstack_chunk_free free
#include "obstack.h"

extern struct grecs_list *prepend_load_path;
extern struct grecs_list *module_load_path; 

void read_names_from_file (const char *filename);
void update_argcv (int *pargc, char ***pargv);
extern int filename_terminator;

const unsigned *fget_word (FILE *fp,
			   int (*ignore) (void *data, FILE *fp, int c),
			   void *data);


void read_badwords (char *name);
int badword_p (const unsigned *s);
void register_exclude_regexp (const char *str);
void read_exclude_regexps (const char *filename);
int excluded_word_p (const char *word);

void config_init(void);
void config_finish (struct grecs_node *tree);
void config_help (void);

extern char *conffile;
extern int lint_mode;
extern int preprocess_only;

int assert_string_arg (grecs_locus_t *locus,
		       enum grecs_callback_command cmd,
		       const grecs_value_t *value);

struct swis_backend_module *swis_backend_load (const char *name);


