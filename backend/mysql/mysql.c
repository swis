/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <sysexits.h>
#include "swis/backend.h"
#include "grecs.h"
#include <mysql/mysql.h>

/* MySQL access parameters */ 
static char *mysql_config_file;
static char *mysql_config_group;

static char *host;                 /* host name */
static char *socket_name;          /* socket ... */
static int port;                   /* ... or port */
static char *db = "swis";          /* Database name */
static char *user;                 /* User name */
static char *pass;                 /* Password */
static char *cacert;

static int cleanup_option;
static int debug_level;

/* Table and field names */
#define COLTYPE_STRING  0
#define COLTYPE_NUMERIC 1
struct table_info
{
  char *name;
  int ncolumns;
  char **col_id;
  char **col_name;
  int *col_type;
};

static int common_types[] = { COLTYPE_STRING, COLTYPE_NUMERIC };
enum { url_column_url, url_column_id };
static char *url_columns[] = { "url", "id" };
static char *url_names[] = { "url", "id" };
static struct table_info url_tab = { "url",
				     2,
				     url_columns,
				     url_names,
				     common_types };

static int url_auto_increment = url_column_id;

enum { word_column_word, word_column_pos, word_column_off, word_column_id };
static char *word_columns[] = { "word", "pos", "off", "url_id" };
static char *word_names[] = { "word", "pos", "off", "url_id" };
static int word_types[] = { COLTYPE_STRING, COLTYPE_NUMERIC, COLTYPE_NUMERIC,
			    COLTYPE_NUMERIC };
static struct table_info word_tab = { "words",
				      4,
				      word_columns,
				      word_names,
				      word_types };

enum { text_column_text, text_column_id };
static char *text_columns[] = { "text", "url_id" };
static char *text_names[] = { "text", "url_id" };
static struct table_info text_tab = { "text",
				      2,
				      text_columns,
				      text_names,
				      common_types };

static int
find_column_id (struct table_info *ti, const char *cid, size_t len)
{
  int i;
  for (i = 0; i < ti->ncolumns; i++)
    if (strncmp (ti->col_id[i], cid, len) == 0 && ti->col_id[i][len] == 0)
      return i;
  return -1;
}

static int
find_column_name (struct table_info *ti, const char *name)
{
  int i;
  for (i = 0; i < ti->ncolumns; i++)
    if (strcmp (ti->col_name[i], name) == 0)
      return i;
  return -1;
}

/* FIXME: Duplicated */
static int
assert_string_arg (grecs_locus_t *locus,
		   enum grecs_callback_command cmd,
		   const grecs_value_t *value)
{
  if (cmd != grecs_callback_set_value)
    {
      grecs_error (locus, 0, _("Unexpected block statement"));
      return 1;
    }
  if (!value || value->type != GRECS_TYPE_STRING)
    {
      grecs_error (&value->locus, 0, _("expected scalar value as a tag"));
      return 1;
    }
  return 0;
}

static int
cb_sql_host (enum grecs_callback_command cmd,
	     grecs_locus_t *locus,
	     void *varptr,
	     grecs_value_t *value,
	     void *cb_data)
{
  char *p;

  if (assert_string_arg (locus, cmd, value))
    return 1;

  p = strchr (value->v.string, ':');
  if (p)
    {
      /* FIXME: Modifies constant string */
      *p++ = 0;
      if (p[0] == '/')
	{
	  socket_name = grecs_strdup (p);
	  host = grecs_strdup ("localhost");
	}
      else
	{
	  char *end;
	  unsigned long n = strtoul (p, &end, 10);
	  if (*end)
	    {
	      grecs_error (&value->locus, 0,
			   "invalid port number (near %s)", end);
	      return 0;
	    }
	  if (n == 0 || n > USHRT_MAX)
	    {
	      grecs_error (&value->locus, 0,
			   "port number out of range 1..%d",
			   USHRT_MAX);
	      return 0;
	    }
	  port = n;
	  /* Save host name */
	  host = grecs_strdup (value->v.string);
	}
    }
  else
    host = grecs_strdup (value->v.string);
  return 0;
}

static int
cb_define_table (enum grecs_callback_command cmd,
		 grecs_locus_t *locus,
		 void *varptr,
		 grecs_value_t *value,
		 void *cb_data)
{
  struct table_info *tip;
  grecs_value_t *arg;
  int i;
  
  if (cmd != grecs_callback_set_value)
    {
      grecs_error (locus, 0, "Unexpected block statement");
      return 1;
    }

  if (!value || value->type != GRECS_TYPE_ARRAY || value->v.arg.c < 2)
    {
      grecs_error (&value->locus, 0, "required values missing");
      return 1;
    }

  arg = value->v.arg.v[0];
  if (arg->type != GRECS_TYPE_STRING)
    {
      grecs_error (&arg->locus, 0, "expected scalar value as table name");
      return 1;
    }

  if (strcmp (arg->v.string, "url") == 0)
    tip = &url_tab;
  else if (strcmp (arg->v.string, "word") == 0)
    tip = &word_tab;
  else if (strcmp (arg->v.string, "text") == 0)
    tip = &text_tab;
  else
    {
      grecs_error (&arg->locus, 0, "unknown table name %s", arg->v.string);
      return 1;
    }

  for (i = 1; i < value->v.arg.c; i++)
    {
      int n;
      size_t len;
      
      arg = value->v.arg.v[i];

      if (arg->type != GRECS_TYPE_STRING)
	{
	  grecs_error (&arg->locus, 0, "expected scalar value");
	  return 1;
	}
      len = strcspn (arg->v.string, ":=");
      switch (arg->v.string[len])
	{
	case 0:
	  grecs_error (&arg->locus, 0,
		       "invalid table specification (near %s)", arg->v.string);
	  return 1;

	case ':':
	  if (strncmp ("name:", arg->v.string, len + 1) == 0)
	    tip->name = strdup (arg->v.string + len + 1);
	  else
	    {
	      grecs_error (&arg->locus, 0,
			   "invalid table specification: expected \"name:\", "
			   "but found %.*s",
			   len, arg->v.string);
	      return 1;
	    }
	  break;
	  
	case '=':
	  n = find_column_id (tip, arg->v.string, len);
	  if (n == -1)
	    grecs_error (&arg->locus, 0,
			 "invalid table specification: no such column %.*s",
			 len, arg->v.string);
	  tip->col_name[n] = strdup (arg->v.string + len + 1);
	}
    }
  return 0;
}   
  
static struct grecs_keyword mysql_kw[] = {
  { "debug", NULL, N_("Enable debugging output"),
    grecs_type_bool, GRECS_DFLT, &debug_level },
  { "config-file", N_("file"), N_("Read MySQL configuration from <file>"),
    grecs_type_string, GRECS_DFLT, &mysql_config_file },
  { "config-group", N_("name"),
    N_("Read the named group from the SQL configuration file"),
    grecs_type_string, GRECS_DFLT, &mysql_config_group },
  { "host", N_("host"), N_("Set SQL server hostname or IP address"),
    grecs_type_string, GRECS_DFLT,
    NULL, 0, cb_sql_host },
  { "database", N_("dbname"), N_("Set database name"),
    grecs_type_string, GRECS_DFLT, &db },
  { "user", N_("name"), N_("Set SQL user name"),
    grecs_type_string, GRECS_DFLT, &user },
  { "password", N_("arg"), N_("Set SQL user password"),
    grecs_type_string, GRECS_DFLT, &pass },
  { "ssl-ca", N_("file"),
    N_("File name of the Certificate Authority (CA) certificate"),
    grecs_type_string, GRECS_DFLT, &cacert },
  { "cleanup", NULL, N_("Clean-up the database"),
    grecs_type_bool, GRECS_DFLT, &cleanup_option },
  { "define-table",
    N_("name: string> <spec: string"),
    N_("Define SQL table.  Specification <spec> is:\n"
       "  [name:<arg: string>]* [<column-id: string>=<column-name: string>]+"),
    grecs_type_string, GRECS_AGGR, NULL, 0, cb_define_table },
  { NULL } 
};

struct escape_slot
{
  char *ptr;
  size_t size;
};

struct mysql_backend
{
  MYSQL mysql;
  MYSQL_RES *result;
  
  unsigned long next_url_id;
  unsigned long url_id;
  unsigned long word_pos;
  struct escape_slot escape_slots[3];
};

static char *
escape_arg (struct mysql_backend *bp, int slot, const char *arg)
{
  size_t arglen = strlen (arg);
  size_t maxlen = arglen * 2 + 1;
  struct escape_slot *ps = &bp->escape_slots[slot];
  
  if (!ps->ptr)
    {
      ps->size = maxlen;
      ps->ptr = grecs_malloc (ps->size);
    }
  else if (ps->size < maxlen)
    {
      ps->size = maxlen;
      ps->ptr = grecs_realloc (ps->ptr, ps->size);
    }

  mysql_real_escape_string (&bp->mysql, ps->ptr, arg, arglen);
  return ps->ptr;
}

static void
exec_sql (struct mysql_backend *bp, const char *fmt, ...)
{
  va_list ap;
  char *qbuf = NULL;
  
  va_start (ap, fmt);
  vasprintf (&qbuf, fmt, ap);
  va_end (ap);
  if (!qbuf)
    {
      fprintf (stderr, "cannot format query\n");
      exit (EX_UNAVAILABLE);
    }

  if (bp->result)
    {
      mysql_free_result (bp->result);
      bp->result = NULL;
    }

  if (debug_level)
    printf ("DEBUG: Query: %s\n", qbuf);
  
  if (mysql_query (&bp->mysql, qbuf))
    {
      fprintf (stderr, "query failed: %s\n", mysql_error (&bp->mysql));
      fprintf (stderr, "the failed query was: %s\n", qbuf);
    }
  else if (debug_level)
    {
      unsigned long x = mysql_affected_rows (&bp->mysql);
      printf ("DEBUG: the query affected %lu rows\n", x);
    }
}

static int
get_numeric_result (struct mysql_backend *bp, unsigned long *var)
{
  unsigned long n;
  MYSQL_ROW row;
  char *p;
  
  if (!bp->result)
    {
      bp->result = mysql_store_result (&bp->mysql);
      if (!bp->result)
	return 1;
    }
  row = mysql_fetch_row (bp->result);
  if (row == NULL || row[0] == NULL)
    return 1;
  n = strtoul (row[0], &p, 10);
  if (*p)
    {
      fprintf (stderr, "cannot convert string to number (near %s)\n", p);
      return 1;
    }
  *var = n;
  return 0;
}

static void
assert_insertion (struct mysql_backend *bp)
{
  if (mysql_affected_rows (&bp->mysql) == 0)
    fprintf (stderr, "query affected 0 rows\n");
}

static void
disconnect (struct mysql_backend *bp)
{
  int i;
  
  for (i = 0; i < sizeof (bp->escape_slots) / sizeof (bp->escape_slots[0]);
       i++)
    free (bp->escape_slots[i].ptr);
  if (bp->result)
    mysql_free_result (bp->result);
  mysql_close (&bp->mysql);
}

static int
connect_to_sql (struct mysql_backend *bp)
{
  mysql_init (&bp->mysql);
  if (mysql_config_file)
    mysql_options (&bp->mysql, MYSQL_READ_DEFAULT_FILE, mysql_config_file);
  if (mysql_config_group)
    mysql_options (&bp->mysql, MYSQL_READ_DEFAULT_GROUP, mysql_config_group);
  if (cacert)
    mysql_ssl_set (&bp->mysql, NULL, NULL, cacert, NULL, NULL);
  
  if (!mysql_real_connect (&bp->mysql, host, user, pass, db, port, socket_name,
			   CLIENT_MULTI_RESULTS))
    {
      fprintf (stderr, "failed to connect to database %s: error: %s\n",
	       db, mysql_error (&bp->mysql));
      return 1;
    }
  exec_sql (bp, "SET NAMES utf8");
  return 0;
}


/* Database verification */
static int
verify_table_sanity (struct mysql_backend *bp,
		     struct table_info *tab, int *pautoinc)
{
  unsigned int num_fields;
  unsigned int i;
  MYSQL_FIELD *field;
  MYSQL_RES *result;
  int *col_ok;
  int errcount = 0;
  
  col_ok = grecs_calloc (tab->ncolumns, sizeof col_ok[0]);
  
  result = mysql_list_fields (&bp->mysql, tab->name, NULL);
  if (!result)
    {
      fprintf (stderr, "cannot get field list for %s.%s: %s\n", db, tab->name,
	       mysql_error (&bp->mysql));
      return 1;
    }

  num_fields = mysql_num_fields (result);
  for (i = 0; i < num_fields; i++)
    {
      int n;

      field = mysql_fetch_field_direct (result, i);
      if (!field)
	{
	  fprintf (stderr,
		   "cannot get description of field %d in table %s\n", i,
		   tab->name);
	  return 1;
	}
      
      n = find_column_name (tab, field->name);
      if (n == -1)
	continue;

      col_ok[n] = 1;

      switch (tab->col_type[n])
	{
	case COLTYPE_STRING:
	  switch (field->type)
	    {
	    case FIELD_TYPE_STRING:
	    case FIELD_TYPE_VAR_STRING:
	    case FIELD_TYPE_BLOB:
	      break;

	    default:
	      errcount++;
	      fprintf (stderr, "column %s.%s is not of string type\n",
		       tab->name, tab->col_name[n]);
	    }
	  break;

	case COLTYPE_NUMERIC:
	  if (!IS_NUM (field->type))
	    {
	      errcount++;
	      fprintf (stderr, "column %s.%s is not of numeric type\n",
		       tab->name, tab->col_name[n]);
	    }
	  if (pautoinc && n == *pautoinc)
	    *pautoinc = field->flags & AUTO_INCREMENT_FLAG;
	}
    }
  mysql_free_result (result);

  for (i = 0; i < tab->ncolumns; i++)
    {
      if (!col_ok[i])
	{
	  fprintf (stderr, "no column %s in table %s\n", tab->col_name[i],
		   tab->name);
	  errcount++;
	}
    }
  free (col_ok);
  return errcount;
}

static void
verify_db_sanity (struct mysql_backend *bp)
{
  if (verify_table_sanity (bp, &url_tab, &url_auto_increment))
    exit (EX_DATAERR);
  if (verify_table_sanity (bp, &word_tab, NULL))
    exit (EX_DATAERR);
  if (verify_table_sanity (bp, &text_tab, NULL))
    exit (EX_DATAERR);
    
  if (!url_auto_increment)
    {
      exec_sql (bp, "SELECT MAX(%s)+1 FROM %s",
		url_tab.col_name[url_column_id], url_tab.name);
      if (get_numeric_result (bp, &bp->next_url_id))
	{
	  fprintf (stderr, "cannot get maximum URL ID: %s\n",
		   mysql_error (&bp->mysql));
	  exit (EX_DATAERR);
	}
    }
}


static swis_backend_handle_t
_mysql_open (struct grecs_node *cfg, int mode)
{
  struct mysql_backend *bp;
  
  if (cfg && grecs_tree_process (cfg, mysql_kw))
    exit (EX_CONFIG);
  bp = grecs_calloc (1, sizeof (*bp));
  if (connect_to_sql (bp))
    {
      disconnect (bp);
      return NULL;
    }

  verify_db_sanity (bp);

  if (cleanup_option)
    {
      exec_sql (bp, "DELETE FROM %s", url_tab.name);
      exec_sql (bp, "DELETE FROM %s", word_tab.name);
      exec_sql (bp, "DELETE FROM %s", text_tab.name);
    }
  
  return (swis_backend_handle_t) bp;
}

static int
_mysql_close (swis_backend_handle_t bh)
{
  struct mysql_backend *bp = (struct mysql_backend *) bh;
  disconnect (bh);
  return 0;
}

static int
_mysql_change_url (swis_backend_handle_t bh, const char *url)
{
  struct mysql_backend *bp = (struct mysql_backend *) bh;
  char *p;
  
  exec_sql (bp,
	    "SELECT %s FROM %s WHERE %s='%s'",
	    url_tab.col_name[url_column_id], url_tab.name,
	    url_tab.col_name[url_column_url],
	    p = escape_arg (bp, 0, url));
  if (get_numeric_result (bp, &bp->url_id))
    {
      if (url_auto_increment)
	{
	  exec_sql (bp,
		    "INSERT INTO %s (%s) VALUES ('%s')",
		    url_tab.name, url_tab.col_name[url_column_url], p);
	  exec_sql (bp, "SELECT LAST_INSERT_ID()");
	  if (get_numeric_result (bp, &bp->url_id))
	    fprintf (stderr, "cannot get URL ID: %s\n",
		     mysql_error (&bp->mysql));
	}
      else
	{
	  bp->url_id = bp->next_url_id++;
	  exec_sql (bp,
		    "INSERT INTO %s (%s,%s) VALUES (%s,%ul)",
		    url_tab.name, url_tab.col_name[url_column_url],
		    url_tab.col_name[url_column_id],
		    p, bp->url_id);
	}
    }
  return 0;
}

static int
_mysql_store_text (swis_backend_handle_t bh, const char *text)
{
  struct mysql_backend *bp = (struct mysql_backend *) bh;

  exec_sql (bp,
	    "INSERT INTO %s (%s,%s) VALUES ('%s',%lu)",
	    text_tab.name, text_tab.col_name[text_column_text],
	    text_tab.col_name[text_column_id],
	    escape_arg (bp, 0, text), bp->url_id);
  assert_insertion (bp);
  return 0;
}

static int
_mysql_store_word (swis_backend_handle_t bh, char *word,
		   unsigned long off, unsigned long n)
{
  struct mysql_backend *bp = (struct mysql_backend *) bh;

  exec_sql (bp,
	    "INSERT INTO %s (%s,%s,%s,%s) VALUES ('%s',%lu,%lu,%lu)",
	    word_tab.name,
	    word_tab.col_name[word_column_word],
	    word_tab.col_name[word_column_pos],
	    word_tab.col_name[word_column_off],
	    word_tab.col_name[word_column_id],
	    escape_arg (bp, 0, word), n, off, bp->url_id);
  assert_insertion (bp);
  return 0;
}

struct swis_backend_module SWIS_EXPORT(mysql, module) = {
  SWIS_BACKEND_VERSION,
  SWIS_DEFAULT_CAPA,
  NULL,
  _mysql_open,
  _mysql_close,
  _mysql_change_url,
  _mysql_store_text,
  _mysql_store_word
};
  
