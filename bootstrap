#! /bin/sh
checkout_only_file=README-hacking

usage() {
  echo >&2 "\
Usage: $0 [OPTION]...
Bootstrap this package from the checked-out sources.

Supported options are:

--force                  Attempt to bootstrap even if the sources seem
                         not to have been checked out.
--gnulib-srcdir=DIRNAME  Specify the local directory where gnulib
                         sources reside. This option is required, unless
                         \`gnulib-tool' is in your PATH.
--help                   Print this help list.
  "
}			     
    
for option
do
  case $option in
  --help)
    usage
    exit 0;;
  --gnulib-srcdir=*)
    GNULIB_SRCDIR=`expr "$option" : '--gnulib-srcdir=\(.*\)'`
    PATH=$GNULIB_SRCDIR:$PATH;;
  --force)
    checkout_only_file=;;
  *)
    echo >&2 "$0: $option: unknown option"
    exit 1;;
  esac
done  	

if test -n "$checkout_only_file" && test ! -r "$checkout_only_file"; then
  echo "$0: Bootstrapping from a non-checked-out distribution is risky." >&2
  exit 1
fi    
   
if ! which gnulib-tool >/dev/null; then
  echo >&2 "$0: gnulib-tool not found; try $0 --help"
  exit 1
fi

test -d m4 || mkdir m4 || exit 1
test -d gnu || mkdir gnu || exit 1

git submodule init
git submodule update

cat > ChangeLog <<EOT
This file is a placeholder. It will be replaced with the actual ChangeLog
by make.  Run make ChangeLog if you wish to create it earlier.
EOT

MODULES=`grep '^[^#]' gnulib.modules`

gnulib-tool --source-base=gnu --import $MODULES &&
 autoreconf -f -i -s

