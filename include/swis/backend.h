/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __swis_backend_h
#define __swis_backend_h

#define __swis_s_cat3__(a,b,c) a ## b ## c
#define SWIS_EXPORT(module,name) __swis_s_cat3__(module,_LTX_,name)

#define SWIS_BACKEND_VERSION 0
#define SWIS_DEFAULT_CAPA 0

#define SWIS_MODE_READ 0
#define SWIS_MODE_WRITE 1

typedef void *swis_backend_handle_t;
struct grecs_node;
struct swis_node;

struct swis_backend_module
{
  unsigned swis_version;
  unsigned swis_capabilities;
  int (*swis_init) (void);
  swis_backend_handle_t (*swis_open) (struct grecs_node *cfg, int mode);
  int (*swis_close) (swis_backend_handle_t);
  int (*swis_change_url) (swis_backend_handle_t bh, const char *url);
  int (*swis_store_text) (swis_backend_handle_t bh, const char *text);
  int (*swis_store_word) (swis_backend_handle_t bh, char *word,
			  unsigned long off, unsigned long n);
  int (*swis_search) (swis_backend_handle_t bh, struct swis_node *query);
};

#endif
