/* This file is part of SWIS
   Copyright (C) 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __swis_query_h
#define __swis_query_h

#include <stddef.h>

enum swis_node_type
  {
    swis_node_op,
    swis_node_word,
    swis_node_seq
  };

enum swis_op_code
  {
    swis_op_or,
    swis_op_and,
    swis_op_not
  };

struct swis_operation
{
  enum swis_op_code opcode;
  struct swis_node *arg[2];
};

struct swis_sequence
{
  struct swis_node *this;
  struct swis_node *next;
};

struct swis_node
{
  enum swis_node_type type;
  union
  {
    char *word;
    struct swis_operation op;
    struct swis_sequence seq;
  } v;
};

#endif
