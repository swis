/* This file is part of SWIS
   Copyright (C) 2007, 2012 Sergey Poznyakoff
 
   SWIS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SWIS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SWIS.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __swis_utf8_h
#define __swis_utf8_h

#include <stddef.h>

int swis_utf8_mbtowc_internal (void *data, int (*read) (void*), unsigned int *pwc);
int swis_utf8_mbtowc (unsigned int *pwc, unsigned char *r, size_t len);
int swis_utf8_wctomb (unsigned char *r, unsigned int wc);
unsigned swis_utf8_toupper (unsigned wc);
int swis_utf8_str_toupper (char *s, size_t len);
unsigned swis_utf8_tolower (unsigned wc);
int swis_utf8_str_tolower (char *s, size_t len);
size_t swis_utf8_strlen (const char *s);

size_t swis_mbutf8_strlen (const unsigned *s);
unsigned *swis_mbutf8_strdup (const unsigned *s);
size_t swis_mbutf8_hash_string (const unsigned *ws, size_t n_buckets);
int swis_mbutf8_strcmp (const unsigned *a, const unsigned *b);

#endif
